<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property string $id
 * @property string|null $titulo
 * @property string|null $texto
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'string', 'max' => 5],
            [['titulo'], 'string', 'max' => 20],
            [['texto'], 'string', 'max' => 150],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
        ];
    }
}
