<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1 class="text-justify">Ejemplo 3 de aplicación</h1>

            <div class="col-12 text-justify">
                <h2>Podemos ver un  ejemplo del funcionamiento de este framework
            </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6">
<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider'=>$datos,
    'columns'=>[
        'id',
        'titulo',
        'texto',
    ],
]);
?>
        </div>
        <div class="col-sm-12 col-md-6"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p></div>
        
    </div>

</div>
