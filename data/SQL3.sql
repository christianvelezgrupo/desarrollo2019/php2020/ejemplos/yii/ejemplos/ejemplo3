﻿CREATE DATABASE IF NOT EXISTS ejemplo3;
  USE ejemplo3;

CREATE TABLE noticias(
id char (5) PRIMARY KEY,
titulo char (20),
texto char (150)
);

INSERT INTO noticias VALUES ('001','atleti campeon','el atletico de madrid consigue su primer champions league en un año que parecía desastrozo'),
('002','fake new','nos acaban de colar una noticia que no es cierta evitar compartirla por favor');